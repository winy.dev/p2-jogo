# Prova 2 de Engenharia de Software

Projeto desenvolvido para a disciplina de Engenharia de Software III, ministrada pelo Profº Me. Alexandre Garcia, do Curso Superior de Tecnologia em Análise e Desenvolvimento de Sistemas, da
Faculdade de Tecnologia da Baixada Santista, 4º ciclo matutino.

## Projeto
Este projeto consiste em jogo e um fórum de discussão onde se discute qualquer assunto relacionado ao jogo. 
O padrão utilizado no projeto foi o MVC através da framework Play![^1].

O jogo é simples. Ele consiste em coletar itens no ambiente e quando todos õs itens são coletados o jogo termina e os pontos são gravados em uma base de dados. O número de itens é gerado aleatoreamente toda vez que o jogo é iniciado.

Para iniciar o jogo, foi utilizado o JQuery[^2] para chamar a função que inicia o jogo.

O menu principal do programa foi criado com o auxílio do Bootstrap[^3].

Para o estilo da página, foi utilizado o SASS[^4].

### O programa
Ao acessar o endereço, o usuário é redirecionado para a página de login. Quando efetuar o login, ele vai para a página principal. A navegação se dá através do menu localizado na parte superior da página. Pode-se escolher entre três opções, **jogar**, entrar no **fórum** ou **sair**.

A opção **Jogar** permite ao usuário jogar o jogo. Na página do jogo aparecem no lado esquerdo os pontos anteriores, caso existam. Ao finalizar o jogo, os pontos são gravados no banco de dados.

A opção **Forum** permite ao usuário acessar as discussões criadas por ele mesmo ou por outros usuários.

A opção **sair** encerra a sessão do usuário.


----------


### Estrutura do programa
##### Controllers
* HomeController: Contoller que gerencia o app no geral.
* UserController: Gerencia operações referentes ao usuário como cadastro, login, logout e formulários de login e cadastro.
* ThreadController: Cuida das atividades relacionadas ao fórum, como, criação, listagem, etc.
* MessageController: Gerencia as mensagens enviadas para o fórum.
* GameController: Sua função é gerenciar as atividades relacionadas ao jogo.
* AuthController: Sua função é verificar se o usuário está autenticado.
* SecurityController: Controla as senhas, gerando hashs e verificando as mesmas.


##### Models
* User(id: Int, username: String, password: String, email: String, lastLoginAttempt: Timestamp, loginAttempts: Int, active: Boolean)
* Thread(id: Int, username: String, data: Timestamp, titulo: String)
* Message(id: Int, threadID: Int, username: String, data: Timestamp, message: String)
* Game(id: Int, data: Timestamp, username: String, pontos: Int)

###### Data Transfer Object
* **UserDTO**
	* UserLogin(username: String, password: String)
	* UserCadastro(username: String, password: String, email: String)
* **ThreadDTO**
	* ThreadCad(titulo: String)
	* Forum(id: List[Int])
* **MessageDTO**
	* MessageCad(message: String)
* **GameDTO**
	* GameCad(message: String)

###### Data Access Object
* **UserDAO**
	* cadastrar(db: Database, user: User): Unit
	* getUser(db: Database, username: String): User
	* gravarTentativasLogin(db: Database, username: String, ts: java.sql.Timestamp, num: Int)
	* resetarTentativasLogin(db: Database, username: String)
* **ThreadDAO**
	* criar(db: Database, thread: Thread): Unit
	* listar(db: Database, lim: Int): MutableList[Thread]
	* getThread(db: Database, id: Int): Thread
* **MessageDAO**
	* criar(db: Database, message: Message): Unit
	* getAll(db: Database, threadID: Int, lim: Int): MutableList[Message]
* **GameDAO**
	* criar(db: Database, game: Game): Unit
	* getAll(db: Database, username: String, lim: Int): MutableList[Game]

##### Views
* index
* forum
* game
* main
* mainmenu
* showThread
* cadastroForm
* loginForm
* threadForm


----------


## Problemas na implementação do projeto
* **Desenvolvimento do jogo**     
Inicialmente, pretendia-se desenvolver um jogo começando do zero criando toda a estrutura que conteria todas as entidades, os componentes, os sistemas, etc. Porém, não consegui implementar a ideia inicial. Dessa forma, decidi utilizar a biblioteca CraftyJS[^5] e desenvolver o jogo utilizando ela. A partir disso foi possível terminar o jogo dentro do prazo.

## Projeto implementado
Algumas partes do projeto foram modificadas, como, por exemplo, a escolha de utilizar uma biblioteca pronta ao invés de desenvolver o jogo do zero.  

Para iniciar o jogo, agora a função chamada inicia o Crafty.

Quando o jogo termina, é realizada a gravação no banco de dados através da função *gravar()*.


----------

## Instalação

### Pré-requisitos
* *sbt*

### Guia de instalação
Primeiramente deve-se clonar o projeto.
```shell
$ cd <diretorio-de-sua-escolha>
$ git clone https://gitlab.com/winy.dev/p2-jogo.git
```

#### Instalando o cliente
Copie o diretório **p2-jogo** para a máquina onde deseja executar o Play!. Pode renomeá-lo como desejar. Siga as instruções de configuração abaixo.


#### Configurando
Para modificar as configurações relacionadas ao banco de dados, modifique o arquivo *conf/application.conf*. Deve-se modificar os valores de HOST, PORT, DATABASE, USERNAME e PASSWORD.     
```shell
db.default.driver=org.postgresql.Driver
db.default.url="jdbc:postgresql://<HOST>:<PORT>/<DATABASE>?sslmode=require"
db.default.user="<USERNAME>"
db.default.password="<PASSWORD>"
```
Exemplo:
```shell
db.default.driver=org.postgresql.Driver
db.default.url="jdbc:postgresql://exemplo.com:5050/sjbs7g38bdus?sslmode=require"
db.default.user="aqownygsbwef"
db.default.password="022kn12ij121u0u983463c29ny9384263489234cn98346324nd"
```

### Executando   
Para executar vá para o diretório principal onde se encontra o arquivo *build.sbt* e execute:     
```shell
$ sbt "run 8080"
```
Após isso, acesse o endereço em um navegador web. Exemplo:     
```shell
$ http://localhost:8080/
```


---------------------------------------------     

##### Imagens de 3ºs     
**Icones usados:**     
favicon: https://www.iconfinder.com/icons/17820/games_pacman_icon     
go-home: icons do linux     

---------------------------------------------     
📎
[^1]: **Play Framework:** https://www.playframework.com/
[^2]: **JQuery:** https://jquery.com/
[^3]: **Bootstrap:** https://getbootstrap.com/
[^4]: **SASS:** https://sass-lang.com/
[^5]: **Crafty Js:** http://craftyjs.com/

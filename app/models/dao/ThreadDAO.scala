package models;

import play.api.db.Database;
import models.Thread;
import scala.collection.mutable.MutableList;

//Data Access Object
object ThreadDAO {

    /**
     * Grava na base de dados uma nova thread.
     */
    def criar(db: Database, thread: Thread): Unit = {
        try {
            db.withConnection{ conn =>
                val ps = conn.prepareStatement("insert into threads(username,data,titulo) values (?,current_timestamp,?)")
                ps.setString(1,thread.username)
                ps.setString(2,thread.titulo)
                ps.execute()
            }
        } catch {
            case e: Throwable => play.api.Logger.error("SQL Exception", e)
        }
    }

    /**
     * Lista todas as threads do forum limitando-se ao valor passado ao parametro lim.
     */
    def listar(db: Database, lim: Int): MutableList[Thread] = {
        val list = MutableList[Thread]()
 
        db.withConnection { conn =>
            val ps = conn.prepareStatement("select *  from threads order by threads.data DESC limit ?")
            ps.setInt(1,lim)
            val res = ps.executeQuery()
            while(res.next()){
                list.+=(Thread(res.getInt(1), res.getString(2), res.getTimestamp(3),  res.getString(4)))
          }
        }
        list
    }

    /**
     * Obtem os dados de uma thread específica.
     */
    def getThread(db: Database, id: Int): Thread = {
        var thread:Thread = null
        db.withConnection { conn =>
            val ps = conn.prepareStatement("select * from threads where id=?")
            ps.setInt(1,id)
            val res = ps.executeQuery()
            while(res.next()){
                thread = new Thread(res.getInt(1),res.getString(2), res.getTimestamp(3), res.getString(4))
            }
        }
        thread
    }
}
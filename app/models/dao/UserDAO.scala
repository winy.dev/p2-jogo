package models;

import play.api.db.Database;
import models.User;

//Data Access Object
object UserDAO {

    /**
     * Grava na base de dados um novo usuário.
     */
    def cadastrar(db: Database, user: User): Unit = {
        try {
            db.withConnection{ conn =>
                val ps = conn.prepareStatement("insert into users(username,password,email,active) values (?,?,?,?)")
                ps.setString(1,user.username)
                ps.setString(2,user.password)
                ps.setString(3,user.email)
                ps.setBoolean(4,user.active)
                ps.execute()
            }
        } catch {
            case e: Throwable => play.api.Logger.error("SQL Exception", e)
        }
    }

    /**
     * Retorna os dados do usuário baseando-se em sua username.
     */
    def getUser(db: Database, username: String): User = {
        var user:User = null
        db.withConnection { conn =>
            val ps = conn.prepareStatement("select password, email,lastLoginAttempt,loginAttempts,active  from users where username=?")
            ps.setString(1,username)
            val res = ps.executeQuery()
            while(res.next()){
                user = new User(0,username, res.getString(1), res.getString(2), res.getTimestamp(3), res.getInt(4), res.getBoolean(5))
            }
                
        }
        user
    }

    /**
     * Grava no banco as tentativas de login. Essa informação será usada a fim de impedir muitas tentativas em um curto período de tempo.
     */
    def gravarTentativasLogin(db: Database, username: String, ts: java.sql.Timestamp, num: Int){
         try {
            db.withConnection{ conn =>
                val ps = conn.prepareStatement("update users set lastLoginAttempt=?,loginAttempts=? where username=?")
                ps.setTimestamp(1,ts)
                ps.setInt(2,num)
                ps.setString(3,username)
                ps.execute()
            }
        } catch {
            case e: Throwable => play.api.Logger.error("SQL Exception", e)
        }
    }

    /**
     * Reseta as informações de login para que o usuário possa logar novamente.
     */
    def resetarTentativasLogin(db: Database, username: String){
         try {
            db.withConnection{ conn =>
                val ps = conn.prepareStatement("update users set lastLoginAttempt=NULL,loginAttempts=? where username=?")
                ps.setInt(1,0)
                ps.setString(2,username)
                ps.execute()
            }
        } catch {
            case e: Throwable => play.api.Logger.error("SQL Exception", e)
        }
    }
    
}
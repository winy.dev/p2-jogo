package models;

import play.api.db.Database;
import models.Game;
import scala.collection.mutable.MutableList;

//Data Access Object
object GameDAO {

    /**
     * Grava na base de dados os pontos do usuário, guardando também a data e hora atuais.
     */
    def criar(db: Database, game: Game): Unit = {
        try {
            db.withConnection{ conn =>
                val ps = conn.prepareStatement("insert into jogo(data,username,pontos) values (current_timestamp,?,?)")
                ps.setString(1,game.username)
                ps.setInt(2,game.pontos)
                ps.execute()
            }
        } catch {
            case e: Throwable => play.api.Logger.error("SQL Exception", e)
        }
    }
    
    /**
     * Retorna todos os pontos do usuário, assim como as respectivas datas. A quantidade de dados retornada é controlada através do parâmetro lim que define o limite.
     */
    def getAll(db: Database, username: String, lim: Int): MutableList[Game] = {
        val list = MutableList[Game]()
        db.withConnection { conn =>
            val ps = conn.prepareStatement("select *  from jogo where username=? order by jogo.data ASC limit ?")
            ps.setString(1,username)
            ps.setInt(2,lim)
            val res = ps.executeQuery()
            while(res.next()){
                list.+= (Game(res.getInt(1),res.getTimestamp(2), res.getString(3), res.getInt(4)))
          }
        }
        list
    }

}
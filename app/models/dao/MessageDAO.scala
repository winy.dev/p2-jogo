package models;

import play.api.db.Database;
import models.Message;
import scala.collection.mutable.MutableList;

//Data Access Object
object MessageDAO {

    /**
     * Grava na base de dados a mensagem do usuário.
     */
    def criar(db: Database, message: Message): Unit = {
        try {
            db.withConnection{ conn =>
                val ps = conn.prepareStatement("insert into messages(threadID,username,data,message) values (?,?,current_timestamp,?)")
                ps.setInt(1,message.threadID)
                ps.setString(2,message.username)
                ps.setString(3,message.message)
                ps.execute()
            }
        } catch {
            case e: Throwable => play.api.Logger.error("SQL Exception", e)
        }
    }

    /**
     * Retorna todas as mensagens de uma thread limitando-se ao valor passado pelo parâmetro lim.
     */
    def getAll(db: Database, threadID: Int, lim: Int): MutableList[Message] = {
        val list = MutableList[Message]()
 
        db.withConnection { conn =>
            val ps = conn.prepareStatement("select *  from messages where threadID=? order by messages.data ASC limit ?")
            ps.setInt(1,threadID)
            ps.setInt(2,lim)
            val res = ps.executeQuery()
            while(res.next()){
                list.+= (Message(res.getInt(1),res.getInt(2), res.getString(3), res.getTimestamp(4), res.getString(5)))
          }
        }
        list
    }

}
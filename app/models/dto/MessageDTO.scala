package models;

import play.api.data.Form
import play.api.data.Forms._

//Data Transfer Object
object MessageDTO {

	case class MessageCad(message: String)

	val messageForm: Form[MessageCad] = Form (
		mapping(
			"message" -> nonEmptyText
		)(MessageCad.apply)(MessageCad.unapply)
	)

}
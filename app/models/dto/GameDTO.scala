package models;

import play.api.data.Form
import play.api.data.Forms._

//Data Transfer Object
object GameDTO {

	case class GameCad(message: String)

	val gameForm: Form[GameCad] = Form (
		mapping(
			"pontos" -> nonEmptyText
		)(GameCad.apply)(GameCad.unapply)
	)

}
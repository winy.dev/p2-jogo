package models;

import play.api.data.Form
import play.api.data.Forms._

//Data Transfer Object
object ThreadDTO {

	case class ThreadCad(titulo: String)
	case class Forum(id: List[Int])

	val threadForm: Form[ThreadCad] = Form (
		mapping(
			"titulo" -> text
		)(ThreadCad.apply)(ThreadCad.unapply)
	)

	val forumForm: Form[Forum] = Form (
			mapping(
				"id" -> list(number)
			)(Forum.apply)(Forum.unapply)
	)

}
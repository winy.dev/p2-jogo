package models;

import play.api.data.Form
import play.api.data.Forms._
import java.sql.Timestamp

//DataTransferObject
object UserDTO {

	case class UserLogin(username: String, password: String)
	case class UserCadastro(username: String, password: String, email: String)

    private def checkLength(str: String, min: Int, max: Int): Boolean = {
        str.length() >= min && str.length() <= max
    }

	val userLoginForm: Form[UserLogin] = Form (
		mapping(
			"username" -> text,
			"password" -> text
		)(UserLogin.apply)(UserLogin.unapply)
	)

	val userCadastroForm: Form[UserCadastro] = Form (
		mapping(
			"username" -> nonEmptyText
				.verifying("Nome de usuário muito grande", str => checkLength(str,0,30)),
			"password" -> nonEmptyText
				.verifying("Senha deve ter entre 8 e 72 caracteres.", str => checkLength(str,8,72)),
			"email" -> nonEmptyText
		)(UserCadastro.apply)(UserCadastro.unapply)
	)

}
package models

import java.sql.Timestamp

case class User(id: Int, username: String, password: String, email: String, lastLoginAttempt: Timestamp, loginAttempts: Int, active: Boolean)

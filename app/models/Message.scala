package models

import java.sql.Timestamp

case class Message(id: Int, threadID: Int, username: String, data: Timestamp, message: String)
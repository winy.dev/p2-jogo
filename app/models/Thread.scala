package models

import java.sql.Timestamp

case class Thread(id: Int, username: String, data: Timestamp, titulo: String)
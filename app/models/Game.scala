package models

import java.sql.Timestamp

case class Game(id: Int, data: Timestamp, username: String, pontos: Int)
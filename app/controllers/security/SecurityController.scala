package controllers

import org.mindrot.jbcrypt.BCrypt

object Security {

	private[this] val work = 15;

	def hashPassword(password: String):String = {
		BCrypt.hashpw(password, BCrypt.gensalt(work))
	}

	def verifyPassword(password: String, hash: String):Boolean = {
		BCrypt.checkpw(password, hash)
	}

}
package controllers

import javax.inject.Inject
import play.api.mvc._
import play.api.mvc.Results._
import scala.concurrent.{ExecutionContext, Future}

class AuthAction @Inject() (parser: BodyParsers.Default)(implicit val context: ExecutionContext)
extends ActionBuilderImpl(parser) {

    override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
        request.session.get("username") match {
            case None => Future.successful(Redirect("/login").withNewSession)
            case Some(u) => block(request)
        }
    }
}

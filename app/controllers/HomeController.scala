package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import routes.javascript._
import play.api.routing._

@Singleton
class HomeController @Inject()(cc: ControllerComponents, authAction: AuthAction) extends AbstractController(cc) {

    /**
     * Carrega a página principal
     */
    def index() = authAction { implicit request: Request[AnyContent] =>
        Ok(views.html.index())
    }

    /**
     * Utilizado nos arquivos javascript
     */
    def javascriptRoutes = Action { implicit request =>
        Ok(
            JavaScriptReverseRouter("jsRoutes")(
                routes.javascript.HomeController.index,
                routes.javascript.GameController.gravar
            )
        ).as("text/javascript")
    }

}

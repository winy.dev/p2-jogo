package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.db.Database;
import models.{Thread,ThreadDTO,ThreadDAO,MessageDAO,MessageDTO}

@Singleton
class ThreadController @Inject()(db: Database, cc: ControllerComponents, authAction: AuthAction) 
extends AbstractController(cc) with play.api.i18n.I18nSupport {

    /**
     * Mostra o formulário para criar uma nova thread
     */
    def threadForm() = authAction { implicit request: Request[AnyContent] =>
        Ok(views.html.threadForm(ThreadDTO.threadForm))
    }

    /**
     * Mostra todas as threads
     */
    def showThreads() = authAction { implicit request: Request[AnyContent] =>
        val lista = ThreadDAO.listar(db,10)
        Ok(views.html.forum(lista))
    }

    /**
     * Mostra uma thread específica
     */
    def showThread(id: Int) = authAction { implicit request: Request[AnyContent] =>
        val thread = ThreadDAO.getThread(db,id)
        val messages = MessageDAO.getAll(db,id,20)
        Ok(views.html.showThread(thread, messages,MessageDTO.messageForm))
    }

    def createThread() = authAction { implicit request: Request[AnyContent] =>
        ThreadDTO.threadForm.bindFromRequest.fold(
            formWithErrors => {
                BadRequest(views.html.threadForm(formWithErrors))
            },
            thread => {
                val user = request.session.get("username") match { case Some(u) => u }
                ThreadDAO.criar(db, new Thread(0,user, null, thread.titulo))
                Redirect("/forum")
            }
        )
    }

}

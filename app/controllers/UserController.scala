package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.db.Database;
import models.{User,UserDTO,UserDAO}
import java.sql.Timestamp
import java.time._
import java.util.Date

@Singleton
class UserController @Inject()(db: Database, cc: ControllerComponents, authAction: AuthAction) 
extends AbstractController(cc) with play.api.i18n.I18nSupport {

    /**
     * Cuida do login do usuário
     */
    def login() = Action { implicit request: Request[AnyContent] =>
        UserDTO.userLoginForm.bindFromRequest.fold(
            formWithErrors => {
                BadRequest(views.html.loginForm(formWithErrors))
            },
            user => {
                val u = UserDAO.getUser(db,user.username)
                if(u != null){
                    var num = u.loginAttempts
                    val now = new Timestamp(System.currentTimeMillis())
                    var last = u.lastLoginAttempt
                    if(last != null){
                        val diff =  ((((now.getTime() - last.getTime()) / 1000) % 3600) / 60)
                        if(diff > 1){
                            UserDAO.resetarTentativasLogin(db,u.username)
                            num = 0
                        }
                    } else {
                        last = now
                    }

                    if(num < 3){
                        if(Security.verifyPassword(user.password,u.password)) {
                            Redirect("/").withSession("username" -> u.username)
                        } else {
                            UserDAO.gravarTentativasLogin(db,user.username,last,u.loginAttempts+1)
                            Redirect("/login").withNewSession
                        }
                    } else {
                        Redirect("/login").flashing("error" -> "Numero de tentativas muito grande")
                    }
                }
                else {
                    Redirect("/login").flashing("error" -> "Usuário ou senha incorretos")
                }
            }
        )
    }

    def logout() = authAction { implicit request: Request[AnyContent] =>
        Redirect("/login").withNewSession
    }

    def loginForm() = Action { implicit request: Request[AnyContent] =>
        Ok(views.html.loginForm(UserDTO.userLoginForm)).withNewSession
    }

    def cadastrar() = Action { implicit request: Request[AnyContent] =>
        UserDTO.userCadastroForm.bindFromRequest.fold(
            formWithErrors => {
                BadRequest(views.html.cadastroForm(formWithErrors))
            },
            user => {
                UserDAO.cadastrar(db, new User(0,user.username, Security.hashPassword(user.password),user.email, null, 0, true))
                Redirect("/login").withNewSession
            }
        )
    }

    def cadastroForm() = Action { implicit request =>
        Ok(views.html.cadastroForm(UserDTO.userCadastroForm)).withNewSession
    }

}

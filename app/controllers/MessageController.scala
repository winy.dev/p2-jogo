package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.db.Database;
import models.{Message,MessageDTO,MessageDAO}
import org.apache.commons.text.StringEscapeUtils.{escapeHtml4,escapeEcmaScript}

@Singleton
class MessageController @Inject()(db: Database, cc: ControllerComponents, authAction: AuthAction) 
extends AbstractController(cc) with play.api.i18n.I18nSupport {

    /**
     * Sua função é substituir as tags html e javascript das mensagens enviadas para o forum.
     */
    private def scapeString(str: String):String = {
        escapeHtml4(escapeEcmaScript(str))
    }

    def createMessage(threadID: Int) = authAction { implicit request: Request[AnyContent] =>
    	MessageDTO.messageForm.bindFromRequest.fold(
            formWithErrors => {
        		Redirect(s"/forum/$threadID").flashing("error" -> "Bad Request")
            },
            message => {
                val user = request.session.get("username") match { case Some(u) => u }
                MessageDAO.criar(db, new Message(0,threadID, user, null, scapeString(message.message)))
        		Redirect(s"/forum/$threadID")
            }
        )
    }

}

package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.db.Database;
import models.{Game,GameDAO,GameDTO}

@Singleton
class GameController @Inject()(db: Database, cc: ControllerComponents, authAction: AuthAction) 
extends AbstractController(cc) with play.api.i18n.I18nSupport {

    def jogar() = authAction { implicit request: Request[AnyContent] =>
        val user = request.session.get("username") match { case Some(u) => u }
    	val lista = GameDAO.getAll(db,user,10)
        Ok(views.html.game(lista))
    }

    def gravar() = authAction { implicit request: Request[AnyContent] =>
    	val pontos = request.body.asFormUrlEncoded.get("pontos").head.toInt
		val user = request.session.get("username") match { case Some(u) => u }
		GameDAO.criar(db, new Game(0,null,user,pontos))
    	val lista = GameDAO.getAll(db,user,10)
        Ok(views.html.game(lista))
    }

}

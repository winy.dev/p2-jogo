CREATE TABLE users (
  username varchar(30) PRIMARY KEY,
  password char(60) NOT NULL,
  email varchar(100),
  lastLoginAttempt Timestamp,
  loginAttempts int,
  active boolean,
  UNIQUE (username)
);

CREATE TABLE threads (
  id SERIAL PRIMARY KEY,
  username varchar(30) NOT NULL REFERENCES users(username),
  data Timestamp NOT NULL,
  titulo varchar(50) NOT NULL
);

CREATE TABLE messages (
  id SERIAL PRIMARY KEY,
  threadID int REFERENCES threads(id),
  username varchar(30) NOT NULL REFERENCES users(username),
  data Timestamp NOT NULL,
  message varchar(500) NOT NULL
);

CREATE TABLE jogo (
  id SERIAL PRIMARY KEY,
  data Timestamp NOT NULL,
  username varchar(30) NOT NULL REFERENCES users(username),
  pontos int
);

name := """p2-jogo"""
organization := "com.fatec"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test

libraryDependencies += jdbc
libraryDependencies += "org.postgresql" % "postgresql" % "9.4.1211"

libraryDependencies += "org.mindrot" % "jbcrypt" % "0.3m"

libraryDependencies += "org.apache.commons" % "commons-text" % "1.6"

// https://www.webjars.org/all
libraryDependencies += "org.webjars" % "jquery" % "3.3.1-1"
libraryDependencies += "org.webjars" % "bootstrap" % "4.1.3"
libraryDependencies += "org.webjars.bower" % "crafty" % "0.8.0"
